import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-showemployees',
  templateUrl: './showemployees.component.html',
  styleUrl: './showemployees.component.css'
})
export class ShowemployeesComponent implements OnInit {
  
  employees: any;
  emailId: any;

  constructor() {

    //Getting emailId from LocalStorage
    this.emailId = localStorage.getItem('emailId');

    this.employees = [
      {empId: 101, empName:'kalyan', salary:1212.12, gender:'Male',   doj:'2018-11-15', country:'India',    emailId:'kalyan@gmail.com', password:'123'},
      {empId: 102, empName:'raju',  salary:2323.23, gender:'Male',   doj:'2017-10-16', country:'China',    emailId:'raju@gmail.com',  password:'123'},
      {empId: 103, empName:'akshay', salary:3434.34, gender:'Male', doj:'2016-09-17', country:'USA',      emailId:'akshay@gmail.com', password:'123'},
      {empId: 104, empName:'shiva',  salary:4545.45, gender:'Male',   doj:'2015-08-18', country:'SriLanka', emailId:'shiva@gmail.com',  password:'123'},
      {empId: 105, empName:'kumar', salary:5656.56, gender:'Male',   doj:'2014-07-19', country:'Nepal',    emailId:'kumar@gmail.com', password:'123'}
    ];
  }

  ngOnInit() {
  }

  

}




