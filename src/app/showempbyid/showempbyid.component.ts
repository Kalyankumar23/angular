import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-showempbyid',
  templateUrl: './showempbyid.component.html',
  styleUrl: './showempbyid.component.css'
})
export class ShowempbyidComponent implements OnInit {
  
  employees: any;
  empId: any;
  emp: any;

  constructor() {
    this.employees = [
      {empId: 101, empName:'kalyan', salary:1212.12, gender:'Male',   doj:'2018-11-15', country:'India',    emailId:'kalyan@gmail.com', password:'123'},
      {empId: 102, empName:'shiva',  salary:2323.23, gender:'Male',   doj:'2017-10-16', country:'China',    emailId:'shiva@gmail.com',  password:'123'},
      {empId: 103, empName:'raju', salary:3434.34, gender:'Male', doj:'2016-09-17', country:'USA',      emailId:'raju@gmail.com', password:'123'},
      {empId: 104, empName:'akshay',  salary:4545.45, gender:'Male',   doj:'2015-08-18', country:'SriLanka', emailId:'akshay@gmail.com',  password:'123'},
    ];
  }

  ngOnInit() {
  }

  getEmpById(employee: any) {
    this.emp = null;
   
    this.employees.forEach((element: any) => {
      if (element.empId == employee.empId) {
        this.emp = element;
      }
    });
  }
  

}
